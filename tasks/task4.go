package main

import (
	"fmt"
)

func main() {
	fmt.Println(MySquareRoot(10, 12))
}

func MySquareRoot(num, precision uint) (result float64) {

	z := 1.0 // initial guess to be 1
	i := 0
	for int(z*z) != int(num) { // until find the first approximation
	   // Newton root algorithm
	   z -= ((z*z - float64(num)) / (2 * z))
	   i++
	}

	return z
}
